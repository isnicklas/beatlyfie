class CreateCategory < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :values, array: true, default: []
    end

    add_index :categories, :name
  end
end
