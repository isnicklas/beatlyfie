# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

{
  arts:        [28, 30, 112, 111].sort,
  music:       [16, 23, 30, 133, 137, 237, 277, 252, 217, 17, 16, 214].sort,
  social:      [198, 196, 148, 147, 200, 199].sort,
  nightlife:   [3, 9, 20, 45, 30, 128, 135, 137, 128, 32, 23, 129, 133, 217, 17, 16].sort,
  food_drinks: [285, 9, 45, 20, 156, 153].sort,
  festivals:   [6, 187, 9, 216, 17, 212, 16].sort
}.each_pair do |category, values|
  Category.create(name: category, values: values)
end