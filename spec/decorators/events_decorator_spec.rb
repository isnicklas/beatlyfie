require 'rails_helper'

RSpec.describe EventsDecorator do
  let(:values) { "1, 2, 3, 4" }

  let(:api_events) do
    [
      {
        "id"    => "1",
        "title" => "Event 1"
      },
      {
        "id"    => "2",
        "title" => "Event 2"
      }
    ]
  end

  subject { EventsDecorator.new(category: :arts) }

  before do
    create(:category, name: :arts, values: values)

    allow(EvvntConnector).to receive(:get_events).and_return(api_events)
  end

  describe '#category_values' do
    it 'returns the category ids' do
      expect(subject.category_values).to eq(values)
    end
  end

  describe '#decorated_events' do
    it 'returns a collection of events' do
      expect(subject.decorated_events.count).to be(2)
    end
  end
end