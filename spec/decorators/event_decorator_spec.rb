require 'rails_helper'

RSpec.describe EventDecorator do
  let(:api_event) do
    {
      "id"    => "1",
      "title" => "Event 1",
      "links" => {
        "Website" => "http://example.com"
      }
    }
  end

  subject { EventDecorator.new(api_event) }

  context 'when decorator receives an event from the api' do
    it 'transforms the event keys to objects' do
      expect(subject).to have_attributes(
        id:    "1",
        title: "Event 1",
        links: { "Website" => "http://example.com" },
        website: "http://example.com"
      )
    end
  end
end