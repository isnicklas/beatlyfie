require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  describe 'GET#index' do
    it 'renders the event index template' do
      get :index

      expect(response).to render_template('events/index')
    end

    it 'returns a status success status code' do
      get :index

      expect(response).to have_http_status(:success)
    end
  end
end
