FactoryGirl.define do
  array_of_values = []

  10.times { array_of_values << Faker::Number.digit }

  factory :category do
    name   { Faker::Hipster.word }
    values { array_of_values  }
  end
end