class EventsDecorator
  attr_reader :category, :max_results, :page

  def initialize(payload = {})
    @category    = payload[:category]
    @max_results = payload[:max_results]
    @page = payload[:page]
  end

  def category_values
    Category.find_by(name: category).try(:values)
  end

  def decorated_events
    return [] if events.body.blank?
    events.body.collect! { |event| EventDecorator.new(event) }

  # filtered.reject { |event| Date.parse(event.start_time) < Date.today }
  end

  private

  def events
    EvvntConnector.get_events(
      category_id: category_values,
      max_results: max_results,
      new_than: Time.now.iso8601,
      country: "GB",
      page: page
    )
  end
end
