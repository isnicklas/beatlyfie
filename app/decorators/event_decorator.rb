require 'ostruct'

class EventDecorator < OpenStruct
  def starting_at
    Time.parse(start_time).strftime('%d %B %Y %I:%M%P')
  end

  def ending_at
    Time.parse(end_time).strftime('%d %B %Y | %I:%M%P')
  end

  def ticket
    links["Tickets"]
  end

  def name_of_venue
     "#{venue['name']}, #{venue['town']}"
  end

  def complete_venue
    [
      venue['name'],
      venue['address_1'],
      venue['address_2'],
      venue['town'],
      venue['country'],
      venue['postcode']
    ].reject(&:blank?).join(', ')
  end
end