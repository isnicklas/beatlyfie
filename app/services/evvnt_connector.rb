module EvvntConnector
  extend self

  def get_events(payload = {})
    connector.get '/events', payload
  end

  def get_event(id, payload={})
    connector.get "/events/#{id}", payload
  end

  private

  def connector
    Faraday.new(url: ENV['EVVNT_URL']) do |builder|
      builder.use FaradayMiddleware::ParseJson, content_type: 'application/json'
      builder.use FaradayMiddleware::ParseJson, content_type: 'application/json'
      builder.use Faraday::Response::Logger, Logger.new('faraday.log')
      builder.use Faraday::Response::RaiseError
      builder.use FaradayMiddleware::FollowRedirects
      builder.use Faraday::Adapter::NetHttp
      builder.basic_auth(
        ENV['EVVNT_KEY'], ENV['EVVNT_PASSWORD']
      )
    end
  end
end