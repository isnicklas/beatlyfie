class EventsController < ApplicationController
  before_action :category

  def index
    @events = EventsDecorator.new(
      category:    params[:category],
      page:        params[:page],
      max_results: 8
    ).decorated_events

    page = params[:page].to_i >= 1 ? params[:page].to_i : 1

    @page = page

    respond_to do |format|
      format.html
      format.js { render layout: false }
    end
  end

  def show
    @event = EventDecorator.new(one_event)
  end

  private

  def one_event
    EvvntConnector.get_event(params[:id]).body
  end

  def category
    @category = params[:category]
  end
end
