class ListingsController < ApplicationController
  def index
    @events = EventsDecorator.new(max_results: 6).decorated_events
  end
end