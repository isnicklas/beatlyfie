# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  latitude  = $('#mapid').data('latitude')
  longitude = $('#mapid').data('longitude')

  features = []

  map = new (ol.Map)(
    layers: [new (ol.layer.Tile)(source: new (ol.source.OSM))]
    target: 'mapid'
    controls: ol.control.defaults(attributionOptions:collapsible: false)
    view: new (ol.View)(
      center: ol.proj.fromLonLat([longitude, latitude])
      zoom: 16
    )
  )
