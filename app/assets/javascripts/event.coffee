$ ->
  $('.loader-bg').hide()
  $(document).ajaxStart ->
    $('.loader-bg').show()
    return
  $(document).ajaxStop ->
    $('.loader-bg').hide()
    $("html, body").animate {scrollTop: 0}, 'slow'
    return
  return