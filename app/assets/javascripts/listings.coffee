$ ->
  $(".display-3").typed
    strings:[
      "Do you wanna party?",
      "See what you missed...",
      "...in pictures...",
      "...and in videos",
      "head in and have fun!"
    ]
    typeSpeed: 10
    loop: true,
    showCursor: false
    backDelay: 3000
